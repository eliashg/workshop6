

  function saveToLocalStorage(key, value) {
    
    localStorage.setItem(key, JSON.stringify(value));
    return true;
  }

  function getFromLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }